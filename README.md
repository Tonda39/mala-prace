# Mala Prace

Repozitář k "malé práci" předmětu SSP. Obsahuje generaci HTML stránek pomocí mkdocs a test kvality Markdown stránek. HTML stránky jsou [zde](https://tonda39.gitlab.io/mala-prace/).

## Závislosti

Závislosti lze získat ze souboru `requirements.txt` a nainstalovat pomocí `pip install -r requirements.txt`.



